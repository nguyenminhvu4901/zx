<?php

namespace Tests\Feature;

use App\Models\ZX;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteZXTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_zx_if_zx_is_exist()
    {
       $this->actingAs(User::factory()->create());
        $zx = ZX::factory()->create()->toArray();

        $response = $this->delete(route('zxs.destroy', ['id' => $zx['id']]));

        $this->assertDatabaseMissing('zx_tables', $zx);  
        $response->assertRedirect(route('zxs.index'));
    }

    /** @test */
    public function authenticated_user_can_not_delete_zx_if_zx_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $zx = -1;

        $response = $this->delete(route('zxs.destroy', ['id' => $zx]));

        $response->assertStatus(404);
    }

     /** @test */
     public function unauthenticated_user_can_not_delete_zx()
     {
        $zx = ZX::factory()->create()->toArray();

        $response = $this->delete(route('zxs.destroy', ['id' => $zx['id']]));

        $response->assertRedirect('/login');
     }
}
