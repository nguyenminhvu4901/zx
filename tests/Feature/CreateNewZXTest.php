<?php

namespace Tests\Feature;

use App\Models\ZX;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateNewZXTest extends TestCase
{
     /** @test */
     public function authenticated_user_can_create_new_zx_if_data_is_valid()
     {
        $this->actingAs(User::factory()->create());
         $zx = ZX::factory()->make()->toArray();
 
         $response = $this->post(route('zxs.store'), $zx);
 
         $response->assertStatus(302);
         $this->assertDatabaseHas('zx_tables', $zx);
         $response->assertRedirect('zxs.index');  
     }

      /** @test */
      public function unauthenticated_user_can_not_create_new_zx()
      {
          $zx = ZX::factory()->make()->toArray();
  
          $response = $this->post(route('zxs.store'), $zx);
  
          $response->assertRedirect('/login'); 
      }

       /** @test */
       public function unauthenticated_user_can_not_see_create_new_zx_view()
       {
           $response = $this->get(route('zxs.create'));
   
           $response->assertRedirect('/login'); 
       }

     /** @test */
     public function authenticated_user_can_not_create_new_zx_if_data_is_not_valid()
     {
        $this->actingAs(User::factory()->create());
        $zx = ZX::factory()->make(['name' => '', 'email' => '', 'birthdate' => ''])->toArray();
 
         $response = $this->from('zxs.create')->post(route('zxs.store'), $zx);
 
         $response->assertRedirect('zxs.create');
         $response->assertSessionHasErrors(['name', 'email', 'birthdate']);
     }

      /** @test */
      public function authenticated_user_can_not_create_new_zx_if_name_is_not_valid()
      {
         $this->actingAs(User::factory()->create());
          $zx = ZX::factory()->make(['name' => ''])->toArray();
  
          $response = $this->from('zxs.create')->post(route('zxs.store'), $zx);
  
          $response->assertRedirect('zxs.create');
          $response->assertSessionHasErrors(['name']);
      }

      /** @test */
      public function authenticated_user_can_not_create_new_zx_if_email_is_not_valid()
      {
         $this->actingAs(User::factory()->create());
          $zx = ZX::factory()->make(['email' => ''])->toArray();
  
          $response = $this->from('zxs.create')->post(route('zxs.store'), $zx);
  
          $response->assertRedirect('zxs.create');
          $response->assertSessionHasErrors(['email']);
      }

       /** @test */
       public function authenticated_user_can_not_create_new_zx_if_birthdate_is_not_valid()
       {
          $this->actingAs(User::factory()->create());
           $zx = ZX::factory()->make(['birthdate' => ''])->toArray();
   
           $response = $this->from('zxs.create')->post(route('zxs.store'), $zx);
   
           $response->assertRedirect('zxs.create');
           $response->assertSessionHasErrors(['birthdate']);
       }
}
