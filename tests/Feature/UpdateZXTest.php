<?php

namespace Tests\Feature;

use App\Models\ZX;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateZXTest extends TestCase
{
   /** @test */
   public function authenticated_user_can_update_zx_if_data_is_valid()
   {
        $this->actingAs(User::factory()->create());
        $zx = ZX::factory()->create()->toArray();

        $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);

        $response->assertStatus(302);
        $this->assertDatabaseHas('zx_tables', $zx);
        $response->assertRedirect(route('zxs.index'));
   }

   /** @test */
   public function unauthenticated_user_can_not_update_zx()
   {
        $zx = ZX::factory()->create()->toArray();

        $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);

        $response->assertRedirect('/login');  
   }

   /** @test */
   public function unauthenticated_user_can_not_see_update_zx_view()
   {
        $zx = ZX::factory()->create()->toArray();

        $response = $this->get(route('zxs.edit', ['id' => $zx['id']]));

        $response->assertRedirect('/login');  
   }

   /** @test */
   public function authenticated_user_can_see_update_zx_view()
   {
        $this->actingAs(User::factory()->create());
        $zx = ZX::factory()->create();

        $response = $this->get(route('zxs.edit', ['id' => $zx->id]));

        $response->assertViewHas(['zX' => $zx]); 
   }

    /** @test */
    public function authenticated_user_can_not_update_zx_if_data_is_not_valid()
    {
            $this->actingAs(User::factory()->create());
            $zx = ZX::factory()->create(['name' => ''])->toArray();
            $zx['email'] = '';
            $zx['birthdate'] = '';

            $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);

            $response->assertRedirect(route('zxs.edit', ['id' => $zx['id']])); 
            $response->assertSessionHasErrors(['name', 'email', 'birthdate']); 
    }

     /** @test */
    public function authenticated_user_can_not_update_zx_if_name_is_not_valid()
    {
             $this->actingAs(User::factory()->create());
             $zx = ZX::factory()->create()->toArray();
             $zx['name'] = '';

             $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);
 
             $response->assertRedirect(route('zxs.edit', ['id' => $zx['id']])); 
             $response->assertSessionHasErrors(['name']);        
    }

     /** @test */
     public function authenticated_user_can_not_update_zx_if_email_is_not_valid()
     {
              $this->actingAs(User::factory()->create());
              $zx = ZX::factory()->create()->toArray();
              $zx['email'] = '';
 
              $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);
  
              $response->assertRedirect(route('zxs.edit', ['id' => $zx['id']])); 
              $response->assertSessionHasErrors(['email']);        
     }

     /** @test */
     public function authenticated_user_can_not_update_zx_if_birthdate_is_not_valid()
     {
              $this->actingAs(User::factory()->create());
              $zx = ZX::factory()->create()->toArray();
              $zx['birthdate'] = '';
 
              $response = $this->from(route('zxs.edit', ['id' => $zx['id']]))->put(route('zxs.update', ['id' => $zx['id']]), $zx);
  
              $response->assertRedirect(route('zxs.edit', ['id' => $zx['id']])); 
              $response->assertSessionHasErrors(['birthdate']);        
     }
}
