<?php

namespace Tests\Feature;

use App\Models\ZX;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListZXTest extends TestCase
{
    /** @test */
    public function user_can_get_list_zx()
    {
        $zx = ZX::factory()->create();

        $response = $this->get(route('zxs.index'));

        $response->assertStatus(200);
        $response->assertViewIs('zxs.index');
        $response->assertSee($zx->get);
        
    }
}
