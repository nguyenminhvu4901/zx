<?php

namespace Tests\Feature;

use App\Models\ZX;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowZXTest extends TestCase
{
     /** @test */
     public function user_can_get_detail_zx_if_zx_is_exist()
     {
         $zx = ZX::factory()->create();
 
         $response = $this->get(route('zxs.show', ['id' => $zx->id]));
 
         $response->assertStatus(200);
         $response->assertViewHas('zX', $zx);
         $response->assertSee($zx->get);    
     }

     /** @test */
     public function user_can_not_get_detail_zx_if_zx_is_not_exist()
     {
         $zx = -1;
 
         $response = $this->get(route('zxs.show', ['id' => $zx]));
 
         $response->assertStatus(404);   
     }
}
