<?php

use App\Http\Controllers\ZXController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/zxs', [ZXController::class, 'index'])->name('zxs.index');

Route::get('/zxs/create', [ZXController::class, 'create'])->name('zxs.create')->middleware('auth');

Route::post('/zxs', [ZXController::class, 'store'])->name('zxs.store')->middleware('auth');

Route::get('/zxs/show/{id}', [ZXController::class, 'show'])->name('zxs.show');

Route::get('/zxs/edit/{id}', [ZXController::class, 'edit'])->name('zxs.edit')->middleware('auth');

Route::put('/zxs/{id}', [ZXController::class, 'update'])->name('zxs.update')->middleware('auth');

Route::delete('/zxs/{id}', [ZXController::class, 'destroy'])->name('zxs.destroy')->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
