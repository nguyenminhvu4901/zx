<?php

namespace App\Http\Controllers;

use App\Models\ZX;
use Illuminate\Http\Request;
use App\Http\Requests\ZX\StoreRequest;
use App\Http\Requests\ZX\UpdateRequest;

class ZXController extends Controller
{
    protected $zX;
    public function __construct(ZX $zX)
    {
        $this->zX = $zX;
    }
    /**
     * Display a listing of the resource.
     *
     * 
     */
    public function index()
    {
        $zxs = $this->zX->all();
        return view('zxs.index', ['zxs'=>$zxs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * 
     */
    public function create()
    {
        return view('zxs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     */
    public function store(StoreRequest $request)
    {
        $zX = $request->all();
        $this->zX->create($zX);
        return redirect('zxs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * 
     */
    public function show($id)
    {
        $zX = $this->zX->findOrFail($id);
        return view('zxs.show', compact('zX'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * 
     */
    public function edit($id)
    {
        $zX = $this->zX->findOrFail($id);
        return view('zxs.edit', compact('zX'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * 
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->all();
        $zX = $this->zX->findOrFail($id);
        $zX->update($data);
        return redirect()->route('zxs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zX = $this->zX->findOrFail($id);
        $zX->delete($id);
        return redirect()->route('zxs.index');
    }
}
