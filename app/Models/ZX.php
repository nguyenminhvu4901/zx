<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZX extends Model
{
    use HasFactory;

    protected $table = 'zx_tables';

    protected $fillable = ['name', 'email', 'birthdate'];
    
}
