<?php

namespace Database\Factories;

use App\Models\ZX;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\WithFaker;

class ZXFactory extends Factory
{
    use WithFaker;
    protected $model = ZX::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'birthdate' => $this->faker->date,
        ];
    }
}
